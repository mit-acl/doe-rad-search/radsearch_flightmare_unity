# RPG Flightmare Unity

RPG Flightmare Unity is a [Unity](https://unity.com/) project for building the photorealistic image renderering engine
of the [RPG Flightmare simulator](https://github.com/uzh-rpg/flightmare). Modified for the radiological search project.

![](Images/manu.png)

## Spawnable Objects

Drone_red

![](Images/Drone_red.png)

rpg_gate

![](Images/rpg_gate.png)

Bottle_Endurance

![](Images/Bottle_Endurance.png)

Bottle_Health

![](Images/Bottle_Health.png)

Bottle_Mana

![](Images/Bottle_Mana.png)

BoxOfPandora

![](Images/BoxOfPandora.png)

CardboardBox_1

![](Images/CardboardBox_1.png)

cliff

![](Images/cliff.png)

Coin

![](Images/Coin.png)

Fence Gate

![](Images/Fence-Gate.png)

Fence Post

![](Images/Fence-Post.png)

Fence

![](Images/Fence.png)

Fortress Gate

![](Images/Fortress-Gate.png)

Mailbox

![](Images/Mailbox.png)

Small Plant

![](Images/Small-Plant.png)

## Getting Started

---

Read the [WIKI](https://github.com/uzh-rpg/rpg_flightmare_unity/wiki) for [installation](https://github.com/uzh-rpg/rpg_flightmare_unity/wiki/Installation-Guide), [usage](https://github.com/uzh-rpg/rpg_flightmare_unity/wiki/Usage-Guide) and [developer](https://github.com/uzh-rpg/rpg_flightmare_unity/wiki/Developer-Guide) guide.

## Acknowledgements

This project is inspired by [FlightGoggles](https://github.com/mit-fast/FlightGoggles), we use some components from FlightGoggles.

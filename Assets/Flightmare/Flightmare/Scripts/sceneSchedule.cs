﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Dynamic scene management
using UnityEngine.SceneManagement;

namespace RPGFlightmare
{
  public class Scenes
  {
    public const string SCENE_WAREHOUSE = "Warehouse/Scenes/DemoScene";
    public const string SCENE_GARAGE = "Garage/Scenes/DemoScene";
    public const string SCENE_TUNELS = "Tunels/Scenes/DemoSceneSimple";
    public const string SCENE_NATUREFOREST = "Forest/Scenes/DemoScene";
    //
    public List<string> scenes_list = new List<string>();
    public int default_scene_id;
    public int num_scene; // number of scenes in total
    public Scenes()
    {
      scenes_list.Add(SCENE_WAREHOUSE);
      scenes_list.Add(SCENE_GARAGE);
      scenes_list.Add(SCENE_TUNELS);
      scenes_list.Add(SCENE_NATUREFOREST);
      default_scene_id = 1;
      num_scene = scenes_list.Count;
    }

  }
  public class sceneSchedule : MonoBehaviour
  {
    public GameObject camera_template; // Main camera
                                       // public GameObject cam_tmp;
    public GameObject[] time_lines;
    public Scenes scenes = new Scenes();
    public void Start()
    {
      loadScene(scenes.default_scene_id, true);
    }
    public void loadScene(int scene_id, bool camera_preview)
    {
      if (scene_id >= 0 && scene_id < scenes.num_scene)
      {
        SceneManager.LoadScene(scenes.scenes_list[scene_id]);
        scenes.default_scene_id = scene_id;
      }
      else
      {
        SceneManager.LoadScene(scenes.scenes_list[scene_id]);
      }

      if (!camera_preview)
      {
        foreach (var tl in time_lines)
        {
          tl.SetActive(false);
        }
      }
    }

    public void loadWareHouse()
    {
      loadScene(0, true);
    }
    public void loadGarage()
    {
      loadScene(1, true);
    }

    public void loadTunel()
    {
      loadScene(2, true);
    }
    public void loadForest()
    {
      loadScene(3, true);
    }

    // public void destoryTimeLine()
    // {
    //     Destroy(time_line);
    //     Destroy(cam_tmp);
    // }

    // public void instantiateCameraCopy()
    // {
    //     // Get object
    //     cam_tmp = GameObject.Instantiate(camera_template);
    //     // Ensure FOV is set for camera.
    //     var currentCam = cam_tmp.GetComponent<Camera>();
    //     currentCam.fieldOfView = 70.0f;
    //     cam_tmp.transform.rotation = Quaternion.identity;
    //     cam_tmp.transform.position = new Vector3(0.0f, 1.0f, 0.0f);
    //     cam_tmp.SetActive(true);
    //     DontDestroyOnLoad(cam_tmp);
    // }

  }
}
